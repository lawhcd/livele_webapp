LiveLe is the Live Lecture Feedback Tool, a webapp that enables real time interaction between lecturers and students.
It is an interactive presentation platform that allows instant but asynchronous communication
for the audience to ask questions without disrupting the speaker's flow,
and allows the speaker to gauge the mood of the audience in order to make on-the-spot adjustments.


LiveLe provides a speaker's control panel, and the audiences' view panel.

Speaker's features:
- Upload slides (pdf format)
- Presentation clicker using any device (eg mobile phone with any internet browser)
- Audience mood bar (on each slide)
- Discreet audience mood bar that only shows on the speaker's view
- Overall mood bar that reflects the speed and/ or tone of presentation
- Audiences' questions on each slide, or as a summary
- Discreet questions view with notifications for popular questions

Audiences' features:
- Browse slides
- Follow the speaker's slides in real time
- Vote on various moods such as speaker's speed and clarity
- Ask questions
- Upvote popular questions

Future features:
- audiences will be able to highlight parts of a slide which they would like the speaker to explain more about
- the aggregated highlights will form a heatmap in the speaker's view

Implementation:
- Backend: Django
- Frontend: Node + Angular with Yeoman dev tools (yo, gulp, bower)